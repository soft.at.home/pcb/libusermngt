# libusermngt

## Summary


The module libusermngt allows fast access to the user datamodel by means of a temporary file.


## Description

When loading the library, the file is mmapped and a the modification time of the file is stored. When the library needs to access the file, it first does a stat to check if it has been updated, recreate the mapping if so, then used the mmapped memory to access the data.

This initial mmap and the final munmap are done using constructor and destructor functions.

To avoid having all the PCB plugins linking against SSL, the function to authenticate users is provided in its own library. SSL is needed there to calculate the SHA-256 checksum of the salt and password/



