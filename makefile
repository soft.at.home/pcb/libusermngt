-include $(CONFIGDIR)/components.config

export INSTALL ?= install
export PKG_CONFIG_LIBDIR ?= /usr/lib/pkgconfig
export BINDIR ?= /usr/bin
export LIBDIR ?= /usr/lib
export SLIBDIR ?= /usr/lib
export LUALIBDIR ?= /usr/lib/lua
export INCLUDEDIR ?= /usr/include
export INITDIR ?= /etc/init.d
export ACLDIR ?= /etc/acl
export DOCDIR ?= $(D)/usr/share/doc/libusermngt
export PROCMONDIR ?= /usr/lib/processmonitor/scripts
export RESETDIR ?= /etc/reset
export MACHINE ?= $(shell $(CC) -dumpmachine)

export COMPONENT = libusermngt

compile:
	$(MAKE) -C src all
ifeq ($(CONFIG_SAH_LIB_USERMNGT_HASHING_TOOL),y)
	$(MAKE) -C tools/src all
endif

clean:
	$(MAKE) -C src clean
	$(MAKE) -C tools/src clean

install:
	$(INSTALL) -D -p -m 0755 src/$(COMPONENT).a $(D)/lib/$(COMPONENT).a
	$(INSTALL) -D -p -m 0755 src/$(COMPONENT)-auth.a $(D)/lib/$(COMPONENT)-auth.a
	$(INSTALL) -d -m 0755 $(D)/$(INCLUDEDIR)/usermngt
	$(INSTALL) -D -p -m 0644 include/usermngt/*.h $(D)$(INCLUDEDIR)/usermngt/
	$(INSTALL) -D -p -m 0644 pkgconfig/usermngt.pc $(PKG_CONFIG_LIBDIR)/usermngt.pc
	$(INSTALL) -D -p -m 0644 pkgconfig/usermngt-auth.pc $(PKG_CONFIG_LIBDIR)/usermngt-auth.pc
ifeq ($(CONFIG_SAH_LIB_USERMNGT_HASHING_TOOL),y)
	$(INSTALL) -D -p -m 0755 tools/src/$(COMPONENT)_app $(D)/bin/$(COMPONENT)_app
endif

.PHONY: compile clean install

