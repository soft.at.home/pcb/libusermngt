/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

#include <openssl/md5.h>
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <debug/sahtrace.h>
#include <usermngt/usermngt.h>
#include <usermngt/auth.h>
#include "usermngt.h"


static bool usermngt_hashMD5Password(const char* password, const char* salt __attribute__ ((unused)), char* hashed) {
    int rc = false;
    char base[USERMNGT_PASSWORD_LENGTH + 1];
    unsigned char md5[MD5_DIGEST_LENGTH];

    snprintf(base, USERMNGT_PASSWORD_LENGTH, "%s", password);

    if(!EVP_Digest(base, strlen(base), md5, NULL, EVP_md5(), NULL)) {
        SAH_TRACE_ERROR("unable to hash password");
        goto error;
    }

    /* convert to hex */
    int i;
    for(i = 0; i < MD5_DIGEST_LENGTH; i++) {
        snprintf(hashed + (i * 2), 3, "%02x", md5[i]);
    }

    rc = true;
error:
    return rc;
}

static bool usermngt_hashSHA512Password(const char* password, const char* salt, char* hashed) {
    int rc = false;
    char base[USERMNGT_SALT_LENGTH + USERMNGT_PASSWORD_LENGTH + 2];
    unsigned char sha[SHA512_DIGEST_LENGTH];

    if(salt && (strlen(salt) != 0)) {
        // FIXME: snprintf size must include the lenghs of separator "|"  and null byte '\0' "USERMNGT_SALT_LENGTH + USERMNGT_PASSWORD_LENGTH + 2".
        snprintf(base, USERMNGT_SALT_LENGTH + USERMNGT_PASSWORD_LENGTH, "%s|%s", salt, password);
    } else {
        snprintf(base, USERMNGT_SALT_LENGTH + USERMNGT_PASSWORD_LENGTH, "%s", password);
    }

    if(!EVP_Digest(base, strlen(base), sha, NULL, EVP_sha512(), NULL)) {
        SAH_TRACE_ERROR("unable to hash password");
        goto error;
    }

    /* convert to hex */
    int i;
    for(i = 0; i < SHA512_DIGEST_LENGTH; i++) {
        snprintf(hashed + (i * 2), 3, "%02x", sha[i]);
    }

    rc = true;
error:
    return rc;
}

static bool usermngt_hashSHA256Password(const char* password, const char* salt, char* hashed) {
    int rc = false;
    char base[USERMNGT_SALT_LENGTH + USERMNGT_PASSWORD_LENGTH + 2];
    unsigned char sha[SHA256_DIGEST_LENGTH];

    if(salt && (strlen(salt) != 0)) {
        // FIXME: snprintf size must include the lenghs of separator "|"  and null byte '\0' "USERMNGT_SALT_LENGTH + USERMNGT_PASSWORD_LENGTH + 2"
        snprintf(base, USERMNGT_SALT_LENGTH + USERMNGT_PASSWORD_LENGTH, "%s|%s", salt, password);
    } else {
        snprintf(base, USERMNGT_SALT_LENGTH + USERMNGT_PASSWORD_LENGTH, "%s", password);
    }

    if(!EVP_Digest(base, strlen(base), sha, NULL, EVP_sha256(), NULL)) {
        SAH_TRACE_ERROR("unable to hash password");
        goto error;
    }

    /* convert to hex */
    int i;
    for(i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        snprintf(hashed + (i * 2), 3, "%02x", sha[i]);
    }

    rc = true;
error:
    return rc;
}

bool usermngt_generateSalt(char salt[SAH_USERMNGT_SALTLENGTH]) {
    bool rc = false;

    int i;

    /* generate a salt */
    char printables[] = SALT_CHAR_PRINTABLES;

    FILE* rand = fopen("/dev/urandom", "r");
    if(rand == NULL) {
        SAH_TRACE_ERROR("unable to open /dev/urandom: %s", strerror(errno));
        goto error;
    }

    for(i = 0; i < SAH_USERMNGT_SALTLENGTH - 1; i++) {
        int c = fgetc(rand);
        if(c < 0) {
            SAH_TRACE_ERROR("unable to read /dev/urandom: %s", strerror(errno));
            goto error_close_rand;
        }
        salt[i] = printables[c % (sizeof(printables) - 1)];
    }
    salt[SAH_USERMNGT_SALTLENGTH - 1] = '\0';

    rc = true;

error_close_rand:
    fclose(rand);
error:
    return rc;
}

bool usermngt_validateSalt(const char* salt, bool allow_empty) {
    unsigned int space_count = 0;
    const char* p = NULL;

    if(salt && (strlen(salt) == 0)) {
        // allow empty salt
        return true;
    }
    if(!salt || (strlen(salt) != SAH_USERMNGT_SALTLENGTH - 1)) {
        SAH_TRACE_INFO("salt or length not correct");
        return false;
    }

    for(p = salt; *p; p++) {
        if(!strchr(SALT_CHAR_PRINTABLES, *p)) {
            if(allow_empty && (*p == ' ')) {
                space_count++;
            } else {
                SAH_TRACE_INFO("Empty char and not allowed here");
                return false;
            }
        }
    }

    /*
     * When empty salt is allowed, there MUST be zero or
     * SAH_USERMNGT_SALTLENGTH-1 spaces.
     */
    if((space_count != 0) && (space_count != SAH_USERMNGT_SALTLENGTH - 1)) {
        SAH_TRACE_INFO("space count wrong: %d", space_count);
        return false;
    }

    return true;
}

bool usermngt_validateHashedPassword(const char* type, const char* hashed) {
    const char* p = NULL;

    if(!hashed) {
        return false;
    }

    if(!strcmp(type, "MD5") || !strcmp(type, "SMD5")) {
        if(strlen(hashed) != MD5_DIGEST_LENGTH * 2) {
            return false;
        }
    } else if(!strcmp(type, "SHA256") || !strcmp(type, "SSHA256")) {
        if(strlen(hashed) != SHA256_DIGEST_LENGTH * 2) {
            return false;
        }
    } else if(!strcmp(type, "SHA512") || !strcmp(type, "SSHA512")) {
        if(strlen(hashed) != SHA512_DIGEST_LENGTH * 2) {
            return false;
        }
    } else if(!strcmp(type, "INVALID")) {
        if((strlen(hashed) != 1) || (*hashed != '*')) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }

    for(p = hashed; *p; p++) {
        if(!isxdigit(*p)) {
            return false;
        }
    }

    return true;
}

bool usermngt_hashPassword(const char* type, const char* password, const char* salt, char hashed[SAH_USERMNGT_HASHLENGTH]) {
    if(strcmp(type, "MD5") == 0) {
        return usermngt_hashMD5Password(password, salt, hashed);
    } else if(strcmp(type, "SSHA256") == 0) {
        return usermngt_hashSHA256Password(password, salt, hashed);
    } else if(strcmp(type, "SSHA512") == 0) {
        return usermngt_hashSHA512Password(password, salt, hashed);
    } else {
        return false;
    }
}

/**
 * @brief
 * Try to authenticate a user
 *
 * @details
 * Check that a username and password pair is valid. If
 * a valid pair is found, the corresponding user is returned.
 *
 * @param username the name of the user to authenticate
 * @param password the password to use for authentication
 * @return the matching user if found, NULL otherwise
 */
const usermngt_user_t* usermngt_authenticate(const char* username, const char* password) {
    INITOK(NULL);

    if((username == NULL) || (*username == '\0')) {
        SAH_TRACEZ_ERROR("usermngt", "invalid user name");
        errno = EINVAL;
        return NULL;
    }

    if((password == NULL) || (*password == '\0')) {
        SAH_TRACEZ_ERROR("usermngt", "invalid user name");
        errno = EINVAL;
        return NULL;
    }

    const usermngt_user_t* user = usermngt_userFindByName(username);
    if((user == NULL) || (user->enable == false)) {
        SAH_TRACEZ_INFO("usermngt", "user unknown or disabled");
        return NULL;
    }

    char hash[SAH_USERMNGT_HASHLENGTH + 1];
    switch(user->passwordType) {
    case USERMNGT_PASSWD_MD5:
        if(!usermngt_hashMD5Password(password, user->salt, hash)) {
            return NULL;
        }
        break;
    case USERMNGT_PASSWD_SSHA256:
        if(!usermngt_hashSHA256Password(password, user->salt, hash)) {
            return NULL;
        }
        break;
    case USERMNGT_PASSWD_SSHA512:
        if(!usermngt_hashSHA512Password(password, user->salt, hash)) {
            return NULL;
        }
        break;
    case USERMNGT_PASSWD_UNKNOWN:
        return NULL;
    }

    if(strcmp(hash, user->password) != 0) {
        return NULL;
    }

    return user;
}
