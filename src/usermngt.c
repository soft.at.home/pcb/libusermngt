/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
 * @file usermngt.c
 * @mainpage User datamodel access library
 *
 * This library provides support for accessing the UserManagement datamodel.
 *
 * The system has many groups and users. A users belongs to many groups. A user
 * as a id, a name and an enable flag. A group as an id, a name and an enable flag.
 *
 * To fetch specific user, see
 * - usermngt_userFindByID() and
 * - usermngt_userFindByName().
 *
 * To loop over all users, see
 * - usermngt_userFirst() and
 * - usermngt_userNext().
 *
 * To fetch specific group, see
 * - usermngt_groupFindByID() and
 * - usermngt_groupFindByName().
 *
 * To loop over all groupss, see
 * - usermngt_groupFirst() and
 * - usermngt_groupNext().
 *
 * To check if a user belongs to a group, see
 * - usermngt_userIsInGroup()
 *
 * To fetch a specific group to which a user belongs to, see
 * - usermngt_userGroupFindByID() and
 * - usermngt_userGroupFindByName().
 *
 * To loop over the groups a user belongs to, see
 * - usermngt_userGroupFirst() and
 * - usermngt_userGroupNext().
 *
 * To authenticate a user, see
 * - usermngt_authenticate().
 */

#include <usermngt/usermngt.h>
#include <debug/sahtrace.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include "usermngt.h"

struct usermngt {
    int iuser;
    int iusergroup;
    int igroup;
    usermngt_model_t* model;
    int error;
    struct timespec passwdChanged;
};

static struct usermngt usermngt;

int usermngt_getLastError() {
    return usermngt.error;
}

int usermngt_usercmp(const void* a, const void* b) {
    return ((usermngt_user_t*) a)->uid - ((usermngt_user_t*) b)->uid;
}

int usermngt_groupcmp(const void* a, const void* b) {
    return ((usermngt_group_t*) a)->gid - ((usermngt_group_t*) b)->gid;
}

static __attribute__ ((constructor)) void usermngt_initialize() {
    struct stat sb;

    SAH_TRACEZ_INFO("usermngt", "initializing library");

    int passwdfd = open(USERMNGT_PASSWD_FILE, O_RDONLY);
    if(passwdfd < 0) {
        SAH_TRACE_NOTICE("unable to open password file %s: %s (is usermngt plugin started ?)",
                         USERMNGT_PASSWD_FILE, strerror(errno));
        goto error;
    }

    if(fstat(passwdfd, &sb) != 0) {
        SAH_TRACE_NOTICE("unable to stat password file %s: %s (is usermngt plugin started ?)",
                         USERMNGT_PASSWD_FILE, strerror(errno));
        goto error;
    }

    if(sizeof(*usermngt.model) != sb.st_size) { /* The file should have the exact same size as the structure */
        SAH_TRACE_NOTICE("%s does not have the expected size (%lld): corrupted file?!?",
                         USERMNGT_PASSWD_FILE, (long long int) sb.st_size);
        errno = EEXIST; /* File exist but the size is incorrect */
        goto error;
    }

    usermngt.passwdChanged.tv_sec = sb.st_mtim.tv_sec;
    usermngt.passwdChanged.tv_nsec = sb.st_mtim.tv_nsec;
    SAH_TRACEZ_INFO("usermngt", "Passwd change time: %lld.%ld", (long long) usermngt.passwdChanged.tv_sec, usermngt.passwdChanged.tv_nsec);
    usermngt.model = mmap(NULL, sizeof(usermngt_model_t), PROT_READ, MAP_PRIVATE, passwdfd, 0);
    if(usermngt.model == MAP_FAILED) {
        SAH_TRACE_NOTICE("unable to access password file %s: %s",
                         USERMNGT_PASSWD_FILE, strerror(errno));
        goto error;
    }
    usermngt.error = 0;

    if(passwdfd >= 0) {
        close(passwdfd);
    }

    return;

error:
    if(passwdfd >= 0) {
        close(passwdfd);
    }
    usermngt.model = MAP_FAILED;
    usermngt.error = errno;
    usermngt.passwdChanged.tv_sec = usermngt.passwdChanged.tv_nsec = 0;
}

static __attribute__ ((destructor)) void usermngt_cleanup() {
    if(usermngt.model != MAP_FAILED) {
        munmap(usermngt.model, sizeof(usermngt_model_t));
    }
    usermngt.error = 0;
    usermngt.passwdChanged.tv_sec = usermngt.passwdChanged.tv_nsec = 0;
}

void usermngt_checkChanges() {
    struct stat sb;
    if(stat(USERMNGT_PASSWD_FILE, &sb) < 0) {
        SAH_TRACE_FATAL("unable to stat %s: %s",
                        USERMNGT_PASSWD_FILE, strerror(errno));
        usermngt.error = errno;
        return;
    }

    SAH_TRACEZ_INFO("usermngt", "passwdChanged %lld.%ld vs sb.st_mtim %lld.%ld",
                    (long long) usermngt.passwdChanged.tv_sec, usermngt.passwdChanged.tv_nsec, (long long) sb.st_mtim.tv_sec, sb.st_mtim.tv_nsec);

    if((sb.st_mtim.tv_sec != usermngt.passwdChanged.tv_sec) || (sb.st_mtim.tv_nsec != usermngt.passwdChanged.tv_nsec)) {
        usermngt_reload();
    }
}


/**
 * @brief
 * Fetch a user by its name
 *
 * @details
 * This function fetches a user matching a name. The usermngt_user_t
 * returned can then be used with the various accessors.
 *
 * If there was an error, NULL is returned and errno is set.
 * If the user cannot be found, errno will be set to ENOENT.
 *
 * The searching algorithm is linear.
 *
 * @param username the name of the user to fetch
 * @return the user matching the name, NULL if it could not be found.
 */
const usermngt_user_t* usermngt_userFindByName(const char* username) {
    INITOK(NULL);

    if((username == NULL) || (*username == '\0')) {
        SAH_TRACEZ_ERROR("usermngt", "invalid user name");
        errno = EINVAL;
        return NULL;
    }

    usermngt_user_t* user = NULL;
    int i;
    for(i = 0; i < usermngt.model->userCount; i++) {
        if(strncmp(usermngt.model->users[i].name, username, USERMNGT_USERNAME_LENGTH) == 0) {
            user = &(usermngt.model->users[i]);
            break;
        }
    }

    if(user == NULL) {
        errno = ENOENT;
    }

    return user;
}

/**
 * @brief
 * Fetch a user by its id
 *
 * @details
 * This function fetches a user matching an id. The usermngt_user_t
 * returned can then be used with the various accessors.
 *
 * If there was an error, NULL is returned and errno is set.
 * If the user cannot be found, errno will be set to ENOENT.
 *
 * The searching algorithm is using a binary search.
 *
 * @param uid the user id of the user to fetch
 * @return the user matching the user id, NULL if it could not be found.
 */
const usermngt_user_t* usermngt_userFindByID(uint32_t uid) {
    INITOK(NULL);

    if(uid == 0) {
        SAH_TRACEZ_ERROR("usermngt", "invalid user id");
        errno = EINVAL;
        return NULL;
    }

    usermngt_user_t key = { .uid = uid, 0 };
    usermngt_user_t* user = bsearch(&key, usermngt.model->users, usermngt.model->userCount,
                                    sizeof(usermngt_user_t), usermngt_usercmp);
    if(user == NULL) {
        errno = ENOENT;
    }

    return user;
}

/**
 * @brief
 * Fetch the first user in the list of user
 *
 * @details
 * This function fetches the first user in the user list.
 *
 * If there was an error, NULL is returned and errno is set
 *
 * @return the first user, NULL if there are no users
 */
const usermngt_user_t* usermngt_userFirst() {
    INITOK(NULL);

    if(usermngt.model->userCount == 0) {
        return NULL;
    }

    usermngt.iuser = 0;
    return usermngt.model->users;
}

/**
 * @brief
 * Fetch the next user in the list of user
 *
 * @details
 * This function fetches the next user in the user list.
 *
 * If there was an error, NULL is returned and errno is set.
 *
 * @return the next user, NULL if there are no more users
 */
const usermngt_user_t* usermngt_userNext() {
    INITOK(NULL);

    if(++usermngt.iuser == usermngt.model->userCount) {
        return NULL;
    }

    return usermngt.model->users + usermngt.iuser;
}

/**
 * @brief
 * Fetch the first group in the user's group list.
 *
 * @details
 * This function fetches the first group in the list of groups a user belongs to.
 *
 * If there was an error, NULL is returned and errno is set
 *
 * @return the first group, NULL if this user does not belong to any group
 */
const usermngt_group_t* usermngt_userGroupFirst(const usermngt_user_t* user) {
    INITOK(NULL);
    if(user->groupCount == 0) {
        return NULL;
    }

    usermngt.iusergroup = 0;
    return user->groups;
}

/**
 * @brief
 * Fetch the next user in the list of user
 *
 * @details
 * This function fetches the next user in the user list.
 *
 * If there was an error, NULL is returned and errno is set.
 *
 * @return the user matching the user id, NULL if there are no more users
 */
const usermngt_group_t* usermngt_userGroupNext(const usermngt_user_t* user) {
    INITOK(NULL);

    if(++usermngt.iusergroup == user->groupCount) {
        return NULL;
    }

    return user->groups + usermngt.iusergroup;
}
/**
 * @brief
 * Fetch a group from a user's group list by its name
 *
 * @details
 * This function fetches a group from the list of groups a user belongs to by
 * matching a group name.
 *
 * If there was an error, NULL is returned and errno is set.
 * If the group cannot be found, errno will be set to ENOENT.
 *
 * The searching algorithm is linear.
 *
 * @param user the user belonging to the group
 * @param groupname the name of the group to fetch
 * @return the group matching the group name, NULL if it could not be found.
 */
const usermngt_group_t* usermngt_userGroupFindByName(const usermngt_user_t* user, const char* groupname) {
    INITOK(NULL);

    if((groupname == NULL) || (*groupname == '\0')) {
        SAH_TRACEZ_ERROR("usermngt", "invalid group name");
        errno = EINVAL;
        return NULL;
    }

    const usermngt_group_t* group = NULL;
    int i;
    for(i = 0; i < user->groupCount; i++) {
        if(strncmp(user->groups[i].name, groupname, USERMNGT_GROUPNAME_LENGTH) == 0) {
            group = &(user->groups[i]);
            break;
        }
    }

    if(group == NULL) {
        errno = ENOENT;
    }

    return group;
}

/**
 * @brief
 * Fetch a group from a user's group list by its id
 *
 * @details
 * This function fetches a group from the list of groups a user belongs to
 * by matching an id.
 *
 * If there was an error, NULL is returned and errno is set.
 * If the user cannot be found, errno will be set to ENOENT.
 *
 * The searching algorithm is using a binary search.
 *
 * @param user the user belonging to the group
 * @param gid the user id of the user to fetch
 * @return the user matching the user id, NULL if it could not be found.
 */
const usermngt_group_t* usermngt_userGroupFindByID(const usermngt_user_t* user, uint32_t gid) {
    INITOK(NULL);

    if(gid == 0) {
        SAH_TRACEZ_ERROR("usermngt", "invalid group id");
        errno = EINVAL;
        return NULL;
    }

    usermngt_group_t key = { .gid = gid, 0 };
    usermngt_group_t* group = bsearch(&key, user->groups, user->groupCount,
                                      sizeof(usermngt_group_t), usermngt_groupcmp);
    if(group == NULL) {
        errno = ENOENT;
    }

    return group;
}

/**
 * @brief
 * Fetch a group by its name
 *
 * @details
 * This function fetches a group matching a name. The usermngt_group_t
 * returned can then be used with the various accessors.
 *
 * If there was an error, NULL is returned and errno is set.
 * If the group cannot be found, errno will be set to ENOENT.
 *
 * The searching algorithm is linear.
 *
 * @param groupname the name of the group to fetch
 * @return the group matching the name, NULL if it could not be found.
 */
const usermngt_group_t* usermngt_groupFindByName(const char* groupname) {
    INITOK(NULL);

    if((groupname == NULL) || (*groupname == '\0')) {
        SAH_TRACEZ_ERROR("usermngt", "invalid group name");
        errno = EINVAL;
        return NULL;
    }

    usermngt_group_t* group = NULL;
    int i;
    for(i = 0; i < usermngt.model->groupCount; i++) {
        if(strncmp(usermngt.model->groups[i].name, groupname, USERMNGT_GROUPNAME_LENGTH) == 0) {
            group = &(usermngt.model->groups[i]);
            break;
        }
    }

    if(group == NULL) {
        errno = ENOENT;
    }

    return group;

}

/**
 * @brief
 * Fetch a group by its id
 *
 * @details
 * This function fetches a group matching an id. The usermngt_group_t
 * returned can then be used with the various accessors.
 *
 * If there was an error, NULL is returned and errno is set.
 * If the group cannot be found, errno will be set to ENOENT.
 *
 * The searching algorithm is using a binary search.
 *
 * @param gid the group id of the group to fetch
 * @return the group matching the group id, NULL if it could not be found.
 */
const usermngt_group_t* usermngt_groupFindByID(uint32_t gid) {
    INITOK(NULL);

    if(gid == 0) {
        SAH_TRACEZ_ERROR("usermngt", "invalid group id");
        errno = EINVAL;
        return NULL;
    }

    usermngt_group_t key = { .gid = gid, 0 };
    usermngt_group_t* group = bsearch(&key, usermngt.model->groups, usermngt.model->groupCount,
                                      sizeof(usermngt_group_t), usermngt_groupcmp);
    if(group == NULL) {
        errno = ENOENT;
    }

    return group;
}


/**
 * @brief
 * Fetch the first group in the list of group
 *
 * @details
 * This function fetches the first group in the group list.
 *
 * If there was an error, NULL is returned and errno is set
 *
 * @return the first group, NULL if there are no groups
 */
const usermngt_group_t* usermngt_groupFirst() {
    INITOK(NULL);

    if(usermngt.model->groupCount == 0) {
        return NULL;
    }

    usermngt.igroup = 0;
    return usermngt.model->groups;
}

/**
 * @brief
 * Fetch the next group in the list of group
 *
 * @details
 * This function fetches the next group in the group list.
 *
 * If there was an error, NULL is returned and errno is set.
 *
 * @return the next group, NULL if there are no more groups
 */
const usermngt_group_t* usermngt_groupNext() {
    INITOK(NULL);

    if(++usermngt.igroup == usermngt.model->groupCount) {
        return NULL;
    }

    return usermngt.model->groups + usermngt.igroup;
}

void usermngt_reload() {
    usermngt_cleanup();
    usermngt_initialize();
}
