/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
/**
 * @file usermngt/usermngt.h
 */
#ifndef USERMNGT_USERMNGT_H
#define USERMNGT_USERMNGT_H

#include <stdbool.h>
#include <stdint.h>
#include <components.h>

#define USERMNGT_USERNAME_LENGTH 64
#define USERMNGT_GROUPNAME_LENGTH 64
#define USERMNGT_PASSWORD_LENGTH 128  // SHA512_DIGEST_LENGTH * 2
#define USERMNGT_SALT_LENGTH 64
#define USERMNGT_USER_GROUP_LENGTH CONFIG_SAH_LIB_USERMNGT_GROUPS_COUNT
#define USERMNGT_USERS_LENGTH CONFIG_SAH_LIB_USERMNGT_USERS_COUNT
#define USERMNGT_GROUPS_LENGTH CONFIG_SAH_LIB_USERMNGT_GROUPS_COUNT
#define USERMNGT_PASSWD_FILE CONFIG_SAH_LIB_USERMNGT_PASSWD_FILE

#ifdef __cplusplus
extern "C" {
#endif

/* this is used to generate the salt */
#define SALT_CHAR_PRINTABLES "1234567890" "abcdef"
#define SALT_CHAR_EMPTY       '\0'

typedef enum usermngt_passwordType {
    USERMNGT_PASSWD_UNKNOWN,
    USERMNGT_PASSWD_MD5,
    USERMNGT_PASSWD_SSHA256,
    USERMNGT_PASSWD_SSHA512
} usermngt_passwordType_t;

typedef struct usermngt_group {
    uint32_t gid;
    bool enable;
    char name[USERMNGT_GROUPNAME_LENGTH + 1];
} usermngt_group_t;

typedef struct usermngt_user {
    uint32_t uid;
    bool enable;
    char name[USERMNGT_USERNAME_LENGTH + 1];
    usermngt_passwordType_t passwordType;
    char password[USERMNGT_PASSWORD_LENGTH + 1];
    char salt[USERMNGT_SALT_LENGTH + 1];
    uint8_t groupCount;
    usermngt_group_t groups[USERMNGT_USER_GROUP_LENGTH + 1];
} usermngt_user_t;

typedef struct usermngt_model {
    uint8_t userCount;
    usermngt_user_t users[USERMNGT_USERS_LENGTH + 1];
    uint8_t groupCount;
    usermngt_group_t groups[USERMNGT_GROUPS_LENGTH + 1];
} usermngt_model_t;

static inline uint32_t usermngt_userID(const usermngt_user_t* user) {
    return user->uid;
}

static inline uint32_t usermngt_userEnable(const usermngt_user_t* user) {
    return user->enable;
}

static inline const char* usermngt_userName(const usermngt_user_t* user) {
    return user->name;
}

static inline uint32_t usermngt_groupID(const usermngt_group_t* group) {
    return group->gid;
}

static inline uint32_t usermngt_groupEnable(const usermngt_group_t* group) {
    return group->enable;
}

static inline const char* usermngt_groupName(const usermngt_group_t* group) {
    return group->name;
}

int usermngt_usercmp(const void* user1, const void* user2);
int usermngt_groupcmp(const void* group1, const void* group2);
const usermngt_user_t* usermngt_userFindByName(const char* user);
const usermngt_user_t* usermngt_userFindByID(uint32_t uid);
const usermngt_user_t* usermngt_userFirst(void);
const usermngt_user_t* usermngt_userNext(void);
const usermngt_group_t* usermngt_userGroupFirst(const usermngt_user_t* user);
const usermngt_group_t* usermngt_userGroupNext(const usermngt_user_t* user);
const usermngt_group_t* usermngt_userGroupFindByName(const usermngt_user_t* user, const char* name);
const usermngt_group_t* usermngt_userGroupFindByID(const usermngt_user_t* user, uint32_t gid);
const usermngt_group_t* usermngt_groupFindByName(const char* user);
const usermngt_group_t* usermngt_groupFindByID(uint32_t gid);
const usermngt_group_t* usermngt_groupFirst(void);
const usermngt_group_t* usermngt_groupNext(void);

#define usermngt_user_for_each_group(group, user) \
    for(group = usermngt_userGroupFirst(user); group; group = usermngt_userGroupNext(user))

#if __STDC_VERSION__ >= 199901L
#define usermngt_user_for_each_declare_group(group, user) \
    for(const usermngt_group_t* group = usermngt_userGroupFirst(user); group; group = usermngt_userGroupNext(user))

#endif


/**
 * Check if a user belongs to a group
 *
 * @param user the user that might belong to the group
 * @param gid the unique id of the group that might contains the user
 * @return true if the user is in the group
 */
static inline bool usermngt_userIsInGroup(const usermngt_user_t* user, uint32_t gid) {
    return (usermngt_userGroupFindByID(user, gid) != (void*) 0);
}

#define SAH_USERMNGT_SALTLENGTH (USERMNGT_SALT_LENGTH + 1)      // SALT size including end of string character
#define SAH_USERMNGT_HASHLENGTH (USERMNGT_SALT_LENGTH + 1 + 64) // Salt + Separator + SHA512_DIGEST_LENGTH

bool usermngt_hashPassword(const char* type, const char* password, const char* salt, char hashed[SAH_USERMNGT_HASHLENGTH]);
bool usermngt_generateSalt(char salt[SAH_USERMNGT_SALTLENGTH]);
bool usermngt_validateSalt(const char* salt, bool allow_empty);
bool usermngt_validateHashedPassword(const char* type, const char* hashed);
void usermngt_reload(void);

#ifdef __cplusplus
}
#endif


#endif
