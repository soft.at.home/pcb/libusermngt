/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>

#include "usermngt/usermngt.h"

#define ME "passwd"

#define SETBIT(mask, offset) { if(0 <= offset)mask |= (2 << offset); }
#define HASBIT(mask, offset) ((0 <= offset) && (0 != (mask & (2 << offset))))

typedef enum {

    usermngtApp_passwd_op_invalid = -1,

    usermngtApp_passwd_op_help,
    usermngtApp_passwd_op_generateSalt,
    usermngtApp_passwd_op_hashPassword,

    usermngtApp_passwd_op_hashType,
    usermngtApp_passwd_op_salt,
    usermngtApp_passwd_op_passwd,

    usermngtApp_passwd_op_canonicalOutput,

} usermngtApp_passwd_op_t;

typedef enum {

    usermngtApp_passwd_retVal_OK = 0,

    usermngtApp_passwd_retVal_syntaxError = 1,
    usermngtApp_passwd_retVal_libError = 2,

    usermngtApp_passwd_retVal_unhandledError = -1,

} usermngtApp_passwd_retVal_t;

static void printUsage(const char* argv0) {

    printf("Usage: %s [Command] [Options...]\n", argv0);
    printf("\n");
    printf("Commands:\n");
    printf("\n");
    printf("  -h, --help\n");
    printf("\n");
    printf("    Just prints this help.\n");
    printf("\n");
    printf("  -g, --generate-salt\n");
    printf("\n");
    printf("    Prints on the standard output the value of an on-the-fly generated Salt.\n");
    printf("    The computed value can be afterwards be passed to command --hash-password through option --salt.\n");
    printf("\n");
    printf("  -a, --hash-password\n");
    printf("\n");
    printf("    Prints on the standard output the Hash of a given ready-to-type password (to be provided with --password).\n");
    printf("    A random Salt will be generated on-the-fly, unless you pass a Salt value through option --salt.\n");
    printf("\n");
    printf("Options:\n");
    printf("\n");
    printf("  --password Password\n");
    printf("\n");
    printf("    Must be passed to command --hash-password to specify the password to be hashed.\n");
    printf("    The given password must not be empty or start with a dash ('-').\n");
    printf("\n");
    printf("  -c, --canonical-output\n");
    printf("\n");
    printf("    May be passed to command --hash-password so that the output is formatted under Softathome's UserManagement canonical form.\n");
    printf("    If you omit that option, the output will omit every field which value is implicit.\n");
    printf("\n");
    printf("  -t, --hash-type Type\n");
    printf("\n");
    printf("    May be passed to command --hash-password to specify the hash algorithm to use. May be either SSHA256 (default) or MD5.\n");
    printf("    The given type must not be empty or start with a dash ('-').\n");
    printf("    NOTE: The type identifier won't be repeated in the output, unless you also pass option --canonical-output.\n");
    printf("\n");
    printf("  -s, --salt Salt\n");
    printf("\n");
    printf("    May be passed to command --hash-password to speciy a given Salt (instead of letting the tool generate on-the-fly a random one).\n");
    printf("    The given Salt must not be empty or start with a dash ('-').\n");
    printf("    NOTE: The Salt's value won't be repeated in the output, unless you also pass option --canonical-output.\n");
    printf("\n");
    printf("Returned values:\n");
    printf("\n");
    printf("  %d: No error.\n", usermngtApp_passwd_retVal_OK);
    printf("\n");
    printf("  %d: Syntax error.\n", usermngtApp_passwd_retVal_syntaxError);
    printf("\n");
    printf("  %d: Error risen by the library.\n", usermngtApp_passwd_retVal_libError);
    printf("\n");
    printf("  %d: Unhandled error.\n", (unsigned char) usermngtApp_passwd_retVal_unhandledError);
    printf("\n");
}

int usermngtApp_applet_passwd(int argc, char* argv[]) {

    usermngtApp_passwd_retVal_t retVal = usermngtApp_passwd_retVal_unhandledError;

    const char* hashType = "SSHA256";
    const char* password = NULL;
    const char* salt = NULL;

    char salt_local[SAH_USERMNGT_SALTLENGTH];
    char hash[SAH_USERMNGT_HASHLENGTH];

    usermngtApp_passwd_op_t opMask = 0;

    int option;

    const struct option long_options[] = {

        { "help", no_argument, 0, 'h' },
        { "generate-salt", no_argument, 0, 'g' },
        { "hash-password", no_argument, 0, 'a' },
        { "canonical-output", no_argument, 0, 'c' },

        { "hash-type", required_argument, 0, 't' },
        { "salt", required_argument, 0, 's' },
        { "password", required_argument, 0, 'p' },

        {0, 0, 0, 0}
    };

    while(-1 != (option = getopt_long(argc, argv, "hgact:s:p:", long_options, NULL))) {

        int optionOffset = usermngtApp_passwd_op_invalid;

        switch(option) {

        case 'h':
            optionOffset = usermngtApp_passwd_op_help;
            break;

        case 'c':
            optionOffset = usermngtApp_passwd_op_canonicalOutput;
            break;

        case 'g':
            optionOffset = usermngtApp_passwd_op_generateSalt;
            break;

        case 'a':
            optionOffset = usermngtApp_passwd_op_hashPassword;
            break;

        case 't':
            optionOffset = usermngtApp_passwd_op_hashType;
            hashType = optarg;
            break;

        case 'p':
            optionOffset = usermngtApp_passwd_op_passwd;
            password = optarg;
            break;

        case 's':
            optionOffset = usermngtApp_passwd_op_salt;
            salt = optarg;
            break;

        default:
            goto syntaxError;
            break;
        }

        SETBIT(opMask, optionOffset);
    }

    if(HASBIT(opMask, usermngtApp_passwd_op_help)) {

        printUsage(argv[0]);
        retVal = usermngtApp_passwd_retVal_OK;
        goto cleanup;
    }

    if(!HASBIT(opMask, usermngtApp_passwd_op_generateSalt)
       && !HASBIT(opMask, usermngtApp_passwd_op_hashPassword)) {
        fprintf(stderr, "No operation specified.\n");
        goto syntaxError;
    }

    if((NULL == salt) && HASBIT(opMask, usermngtApp_passwd_op_hashPassword)) {
        SETBIT(opMask, usermngtApp_passwd_op_generateSalt);
    }

    if(HASBIT(opMask, usermngtApp_passwd_op_generateSalt)) {

        if(NULL != salt) {
            fprintf(stderr, "Salt specified while asked to generate one.\n");
            goto syntaxError;
        }

        memset(salt_local, 0, sizeof(salt_local));

        if(false == usermngt_generateSalt(salt_local)) {
            fprintf(stderr, "Failed: usermngt_generateSalt\n");
            goto libError;
        }

        salt = &salt_local[0];
    }

    if(HASBIT(opMask, usermngtApp_passwd_op_hashPassword)) {

        if(NULL == password) {
            fprintf(stderr, "No Password specified.\n");
            goto syntaxError;
        }
        if('-' == *salt) {
            fprintf(stderr, "Invalid Salt specified: salt must not start with dash.\n");
            goto syntaxError;
        }
        if('-' == *hashType) {
            fprintf(stderr, "Invalid Type specified: type must not start with dash.\n");
            goto syntaxError;
        }

        memset(hash, 0, sizeof(hash));

        if(false == usermngt_hashPassword(hashType, password, salt, hash)) {
            fprintf(stderr, "Failed: usermngt_hashPassword\n");
            //goto libError;
        }
    }

    if(HASBIT(opMask, usermngtApp_passwd_op_hashPassword)
       && HASBIT(opMask, usermngtApp_passwd_op_canonicalOutput)) {
        printf("{%s}", hashType);
    }

    if((salt_local == salt)
       || HASBIT(opMask, usermngtApp_passwd_op_canonicalOutput)) {

        printf("%s", salt);

        if(HASBIT(opMask, usermngtApp_passwd_op_hashPassword)) {
            printf(":");
        }
    }

    if(HASBIT(opMask, usermngtApp_passwd_op_hashPassword)) {
        printf("%s", hash);
    }

    printf("\n");

    retVal = usermngtApp_passwd_retVal_OK;
    goto cleanup;

syntaxError:

    retVal = usermngtApp_passwd_retVal_syntaxError;
    fprintf(stderr, "Syntax error. For usage, type: %s --help\n", argv[0]);
    goto cleanup;

libError:

    retVal = usermngtApp_passwd_retVal_libError;
    fprintf(stderr, "Library error.\n");
    goto cleanup;

cleanup:

    if(0 > retVal) {
        fprintf(stderr, "Unhandled error.\n");
    }

    return retVal;
}

/* This source file was first arranged so that it can be used as an applet,
 * to be called by its entry point with main()-like syntax,
 *
 * from an actual "main()" function that would match (against the names list of
 * the supported applets) either argv[0] (case of an invokation through a symlink
 * named as the applet) and/or argv[1] (no symlink, passing as 1st argument the
 * name of the applet).
 *
 * The code below is just a minimal replacement of that hub "main()".
 */
#if (!defined LIBUSERMNGT_APP_USEHUB)

int main(int argc, char* argv[]) {

    if((2 > argc) || (strcmp(ME, argv[1]))) {
        fprintf(stderr, "Supported applets:\n "ME "\n");
        return -1;
    }

    return usermngtApp_applet_passwd(argc - 1, argv + 1);
}

#endif /* LIBUSERMNGT_APP_USEHUB */

